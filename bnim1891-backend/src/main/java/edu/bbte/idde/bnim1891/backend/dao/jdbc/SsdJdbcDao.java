package edu.bbte.idde.bnim1891.backend.dao.jdbc;

import edu.bbte.idde.bnim1891.backend.dao.SsdDao;
import edu.bbte.idde.bnim1891.backend.model.Ssd;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class SsdJdbcDao extends AbstractJdbcDao<Ssd> implements SsdDao {

    public static final Logger LOG = LoggerFactory.getLogger(SsdJdbcDao.class);

    public SsdJdbcDao() throws IOException {
        super();
    }

    @Override
    public void insert(Ssd entity) {
        Connection connection = null;
        try {

            connection = dataSource.getConnection();
            String query =
                    "insert into Ssd(brandName, price, storageCapacity,"
                            + " readSpeed, writeSpeed) values(?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, entity.getBrandName());
            preparedStatement.setDouble(2, entity.getPrice());
            preparedStatement.setInt(3, entity.getStorageCapacity());
            preparedStatement.setInt(4, entity.getReadSpeed());
            preparedStatement.setInt(5, entity.getWriteSpeed());
            preparedStatement.executeUpdate();
            LOG.info("insert");

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
        } finally {
            if (connection != null) {
                dataSource.evictConnection(connection);
            }
        }
    }

    @Override
    public void delete(long id) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            String query = "delete from Ssd where ssdId = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            LOG.info("delete");
        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
        } finally {
            if (connection != null) {
                dataSource.evictConnection(connection);
            }
        }
    }

    @Override
    public Ssd read(long id) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            String query = "select * from Ssd where ssdId = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            ResultSet set = preparedStatement.executeQuery();
            if (set.next()) {
                Ssd ssd = new Ssd(set.getString(2), set.getDouble(3), set.getInt(4), set.getInt(5), set.getInt(6));
                ssd.setId(set.getLong(1));
                return ssd;
            }
            LOG.info("read");

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
        } finally {
            if (connection != null) {
                dataSource.evictConnection(connection);
            }
        }
        return null;
    }

    @Override
    public void update(long id, Ssd entity) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            String query =
                    "update Ssd set brandName = ?, price = ?, storageCapacity = ?,"
                            + " readSpeed = ?, writeSpeed = ? where ssdId = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, entity.getBrandName());
            preparedStatement.setDouble(2, entity.getPrice());
            preparedStatement.setInt(3, entity.getStorageCapacity());
            preparedStatement.setInt(4, entity.getReadSpeed());
            preparedStatement.setInt(5, entity.getWriteSpeed());
            preparedStatement.setDouble(6, id);
            preparedStatement.executeUpdate();
            LOG.info("update");
        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
        } finally {
            if (connection != null) {
                dataSource.evictConnection(connection);
            }
        }
    }

    @Override
    public Collection<Ssd> readAll() {
        Connection connection = null;
        Collection<Ssd> ssds = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            String query = "select * from Ssd";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet set = preparedStatement.executeQuery();
            while (set.next()) {
                Ssd ssd = new Ssd(set.getString(2), set.getDouble(3), set.getInt(4), set.getInt(5), set.getInt(6));
                ssd.setId(set.getLong(1));
                ssds.add(ssd);
            }
            LOG.info("readAll");

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
        } finally {
            if (connection != null) {
                dataSource.evictConnection(connection);
            }
        }
        return ssds;
    }

    @Override
    public Ssd findByPrice(Double price) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            String query = "select * from Ssd where price = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setDouble(1, price);
            ResultSet set = preparedStatement.executeQuery();
            if (set.next()) {
                Ssd ssd = new Ssd(set.getString(2), set.getDouble(3), set.getInt(4), set.getInt(5), set.getInt(6));
                ssd.setId(set.getLong(1));
                return ssd;
            }
            LOG.info("findByPrice");
        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
        } finally {
            if (connection != null) {
                dataSource.evictConnection(connection);
            }
        }
        return null;
    }
}
