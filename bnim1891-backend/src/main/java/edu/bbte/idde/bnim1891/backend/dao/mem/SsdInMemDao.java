package edu.bbte.idde.bnim1891.backend.dao.mem;

import edu.bbte.idde.bnim1891.backend.dao.SsdDao;
import edu.bbte.idde.bnim1891.backend.model.Ssd;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class SsdInMemDao extends AbstractInMemDao<Ssd> implements SsdDao {

    public SsdInMemDao() {
        super();
        log = LoggerFactory.getLogger(SsdInMemDao.class);
        log.info("SsdInMemDao constructor executed");
        database = new ConcurrentHashMap<>();
        primaryKey = new AtomicLong(0L);
        this.insert(new Ssd("Kingston",158.85,240,500,350));
        this.insert(new Ssd("ADATA", 102.00, 120, 520, 320));
        this.insert(new Ssd("Kingston", 200.99, 140, 530, 340));
    }

    @Override
    public Ssd findByPrice(Double price) {
        Collection<Ssd> collection = database.values();
        for (Ssd ssd :collection) {
            if (ssd.getPrice().equals(price)) {
                return ssd;
            }
        }
        log.info("findByPrice executed");
        return null;
    }
}
