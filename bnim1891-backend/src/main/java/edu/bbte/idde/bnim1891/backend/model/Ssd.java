package edu.bbte.idde.bnim1891.backend.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Ssd extends BaseEntity {
    private String brandName;
    private Double price;
    private Integer storageCapacity;
    private Integer readSpeed;
    private Integer writeSpeed;

}
