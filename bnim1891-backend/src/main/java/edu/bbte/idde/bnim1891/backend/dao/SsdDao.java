package edu.bbte.idde.bnim1891.backend.dao;

import edu.bbte.idde.bnim1891.backend.model.Ssd;

public interface SsdDao extends Dao<Ssd> {
    Ssd findByPrice(Double price);
}
