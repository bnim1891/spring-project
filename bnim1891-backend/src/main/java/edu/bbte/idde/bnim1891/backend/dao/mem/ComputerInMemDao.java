package edu.bbte.idde.bnim1891.backend.dao.mem;

import edu.bbte.idde.bnim1891.backend.dao.ComputerDao;
import edu.bbte.idde.bnim1891.backend.model.Computer;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class ComputerInMemDao extends AbstractInMemDao<Computer> implements ComputerDao {

    public ComputerInMemDao() {
        super();
        database = new ConcurrentHashMap<>();
        primaryKey = new AtomicLong(0L);
        log = LoggerFactory.getLogger(ComputerInMemDao.class);
        log.info("ComputerInMemDao constructor executed");
        this.insert(new Computer("Asus", 23488.90, 1L));
        this.insert(new Computer("Dell", 2560.90, 1L));
        this.insert(new Computer("Lenovo", 4580.90, 1L));
    }

    @Override
    public Computer findByPrice(Double price) {
        Collection<Computer> collection = database.values();
        for (Computer computer :collection) {
            if (computer.getPrice().equals(price)) {
                return computer;
            }
        }
        log.info("findByPrice executed");
        return null;
    }
}
