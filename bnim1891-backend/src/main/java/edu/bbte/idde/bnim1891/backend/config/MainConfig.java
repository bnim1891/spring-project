package edu.bbte.idde.bnim1891.backend.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MainConfig {
    private String daoType;
    @JsonProperty("dataSource")
    private DataSourceConfig dataSourceConfig;
}
