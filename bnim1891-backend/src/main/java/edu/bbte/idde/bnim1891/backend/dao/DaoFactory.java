package edu.bbte.idde.bnim1891.backend.dao;

public interface DaoFactory {

    SsdDao getSsdDao();

    ComputerDao getComputerDao();
}
