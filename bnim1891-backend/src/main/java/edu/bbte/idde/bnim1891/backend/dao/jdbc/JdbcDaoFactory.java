package edu.bbte.idde.bnim1891.backend.dao.jdbc;

import edu.bbte.idde.bnim1891.backend.dao.ComputerDao;
import edu.bbte.idde.bnim1891.backend.dao.DaoFactory;
import edu.bbte.idde.bnim1891.backend.dao.SsdDao;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class JdbcDaoFactory implements DaoFactory {

    private SsdDao jdbcSsdDao;
    private ComputerDao jdbcComputerDao;

    @Override
    public SsdDao getSsdDao() {
        if (jdbcSsdDao == null) {
            try {
                jdbcSsdDao = new SsdJdbcDao();
            } catch (IOException e) {
                log.error(String.valueOf(e));
            }
        }
        return jdbcSsdDao;
    }

    @Override
    public ComputerDao getComputerDao() {
        if (jdbcComputerDao == null) {
            try {
                jdbcComputerDao = new ComputerJdbcDao();
            } catch (IOException e) {
                log.error(String.valueOf(e));
            }
        }
        return jdbcComputerDao;

    }
}
