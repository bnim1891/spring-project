package edu.bbte.idde.bnim1891.backend.dao.mem;

import edu.bbte.idde.bnim1891.backend.dao.ComputerDao;
import edu.bbte.idde.bnim1891.backend.dao.DaoFactory;
import edu.bbte.idde.bnim1891.backend.dao.SsdDao;

public class MemDaoFactory implements DaoFactory {

    private SsdDao memSsdDao;
    private ComputerDao memComputerDao;

    @Override
    public SsdDao getSsdDao() {
        if (memSsdDao == null) {
            memSsdDao = new SsdInMemDao();
        }
        return memSsdDao;
    }

    @Override
    public ComputerDao getComputerDao() {
        if (memComputerDao == null) {
            memComputerDao = new ComputerInMemDao();
        }
        return memComputerDao;
    }

}

