package edu.bbte.idde.bnim1891.backend.dao;

import edu.bbte.idde.bnim1891.backend.config.ConfigFactory;
import edu.bbte.idde.bnim1891.backend.dao.jdbc.JdbcDaoFactory;
import edu.bbte.idde.bnim1891.backend.dao.mem.MemDaoFactory;

import java.io.IOException;

public abstract class AbstractDaoFactory {

    private static DaoFactory daoFactory;

    public static synchronized DaoFactory getDaoFactory() throws IOException {
        if (daoFactory == null) {
            String daoType = ConfigFactory.getMainConfig().getDaoType();
            if ("jdbc".equals(daoType)) {
                daoFactory = new JdbcDaoFactory();
            } else {
                daoFactory = new MemDaoFactory();
            }

        }

        return daoFactory;
    }
}

