package edu.bbte.idde.bnim1891.backend.dao.mem;

import edu.bbte.idde.bnim1891.backend.dao.Dao;
import edu.bbte.idde.bnim1891.backend.model.BaseEntity;
import org.slf4j.Logger;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public abstract class AbstractInMemDao<T extends BaseEntity> implements Dao<T> {
    protected ConcurrentHashMap<Long, T> database;
    protected AtomicLong primaryKey;
    protected Logger log;

    @Override
    public void insert(T entity) {
        log.info("insert executed");
        entity.setId(primaryKey.getAndIncrement());
        database.put(entity.getId(), entity);
    }

    @Override
    public void delete(long id) {
        log.info("delete executed");
        database.remove(id);
    }

    @Override
    public T read(long id) {
        log.info("read executed");
        return database.get(id);
    }

    @Override
    public void update(long id, T entity) {
        log.info("update executed");
        database.computeIfPresent(id, (key, val) -> entity);
    }

    @Override
    public Collection<T> readAll() {
        log.info("readAll executed");
        return database.values();
    }

}
