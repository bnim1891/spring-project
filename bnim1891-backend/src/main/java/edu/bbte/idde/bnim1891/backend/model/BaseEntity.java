package edu.bbte.idde.bnim1891.backend.model;

public class BaseEntity {
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Ssd{"
                + "id=" + id
                + '}';
    }
}
