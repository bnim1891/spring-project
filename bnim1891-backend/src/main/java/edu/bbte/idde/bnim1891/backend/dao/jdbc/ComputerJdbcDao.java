package edu.bbte.idde.bnim1891.backend.dao.jdbc;

import edu.bbte.idde.bnim1891.backend.dao.ComputerDao;
import edu.bbte.idde.bnim1891.backend.model.Computer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class ComputerJdbcDao extends AbstractJdbcDao<Computer> implements ComputerDao {

    public static final Logger LOG = LoggerFactory.getLogger(ComputerJdbcDao.class);

    public ComputerJdbcDao() throws IOException {
        super();
    }

    @Override
    public void insert(Computer entity) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            String query =
                    "insert into Computer(brandName, price, ssdId) values(?, ?, ?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, entity.getBrandName());
                preparedStatement.setDouble(2, entity.getPrice());
                preparedStatement.setLong(3, entity.getSsdId());
                preparedStatement.executeUpdate();
            }

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
        } finally {
            if (connection != null) {
                dataSource.evictConnection(connection);
            }
        }
    }

    @Override
    public void delete(long id) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            String query = "delete from Computer where computerId = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
        } finally {
            if (connection != null) {
                dataSource.evictConnection(connection);
            }
        }
    }

    @Override
    public Computer read(long id) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            String query =
                    "select computerId, brandName, price, ssdId from Computer where computerId = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, id);
                ResultSet set = preparedStatement.executeQuery();
                if (set.next()) {
                    Computer computer = new Computer(set.getString(2), set.getDouble(3), set.getLong(4));
                    computer.setId(set.getLong(1));
                    return computer;
                }
            }

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
        } finally {
            if (connection != null) {
                dataSource.evictConnection(connection);
            }
        }
        return null;
    }

    @Override
    public void update(long id, Computer entity) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            String query =
                    "update Computer set brandName = ?, price = ?, ssdId = ? where computerId = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, entity.getBrandName());
                preparedStatement.setDouble(2, entity.getPrice());
                preparedStatement.setLong(3, entity.getSsdId());
                preparedStatement.setLong(4, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
        } finally {
            if (connection != null) {
                dataSource.evictConnection(connection);
            }
        }
    }

    @Override
    public Collection<Computer> readAll() {
        Collection<Computer> computers = new ArrayList<>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            String query = "select * from Computer";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                ResultSet set = preparedStatement.executeQuery();
                while (set.next()) {
                    Computer computer = new Computer(set.getString(2), set.getDouble(3), set.getLong(4));
                    computer.setId(set.getLong(1));
                    computers.add(computer);
                }
            }

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
        } finally {
            if (connection != null) {
                dataSource.evictConnection(connection);
            }
        }
        return computers;
    }

    @Override
    public Computer findByPrice(Double price) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            String query = "select * from Computer where price = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setDouble(1, price);
                ResultSet set = preparedStatement.executeQuery();
                if (set.next()) {
                    Computer computer = new Computer(set.getString(2), set.getDouble(3), set.getLong(4));
                    computer.setId(set.getLong(1));
                    return computer;
                }
            }

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
        } finally {
            if (connection != null) {
                dataSource.evictConnection(connection);
            }
        }
        return null;
    }
}
