package edu.bbte.idde.bnim1891.backend.config;

import lombok.Data;

@Data
public class DataSourceConfig {
    private String driverClassName;
    private String jdbcUrl;
    private Integer poolSize;
    private String userName;
    private String password;
}
