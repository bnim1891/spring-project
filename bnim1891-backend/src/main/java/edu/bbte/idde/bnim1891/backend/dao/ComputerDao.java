package edu.bbte.idde.bnim1891.backend.dao;

import edu.bbte.idde.bnim1891.backend.model.Computer;

public interface ComputerDao extends Dao<Computer> {
    Computer findByPrice(Double price);
}
