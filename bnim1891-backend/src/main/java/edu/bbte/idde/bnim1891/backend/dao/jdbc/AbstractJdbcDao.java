package edu.bbte.idde.bnim1891.backend.dao.jdbc;

import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.bnim1891.backend.config.ConfigFactory;
import edu.bbte.idde.bnim1891.backend.config.DataSourceConfig;
import edu.bbte.idde.bnim1891.backend.dao.Dao;
import edu.bbte.idde.bnim1891.backend.model.BaseEntity;

import java.io.IOException;
import java.util.Collection;

public abstract class AbstractJdbcDao<T extends BaseEntity> implements Dao<T> {
    protected final HikariDataSource dataSource;

    public AbstractJdbcDao() throws IOException {
        this.dataSource = new HikariDataSource();
        DataSourceConfig dataSourceConfig = ConfigFactory.getMainConfig().getDataSourceConfig();
        dataSource.setDriverClassName(dataSourceConfig.getDriverClassName());
        dataSource.setJdbcUrl(dataSourceConfig.getJdbcUrl());
        dataSource.setMaximumPoolSize(dataSourceConfig.getPoolSize());
        dataSource.setUsername(dataSourceConfig.getUserName());
        dataSource.setPassword(dataSourceConfig.getPassword());
    }

    @Override
    public abstract void insert(T entity);

    @Override
    public abstract void delete(long id);

    @Override
    public abstract T read(long id);

    @Override
    public abstract void update(long id, T entity);

    @Override
    public abstract Collection<T> readAll();
}
