package edu.bbte.idde.bnim1891.backend.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class ConfigFactory {
    private static MainConfig mainConfig;

    public static synchronized MainConfig getMainConfig() throws IOException {
        if (mainConfig == null) {
            String profile = System.getProperty("profile");
            StringBuilder stringBuilder = new StringBuilder("/config-");
            if (profile == null) {
                profile = "dev";
            }
            stringBuilder.append(profile);
            stringBuilder.append(".json");
            log.info("Config file: {}", stringBuilder.toString());
            ObjectMapper objectMapper = new ObjectMapper();
            mainConfig =
                    objectMapper.readValue(
                            ConfigFactory.class.getResourceAsStream(stringBuilder.toString()),
                            MainConfig.class
                    );
            if (mainConfig == null) {
                mainConfig =
                        objectMapper.readValue(
                                ConfigFactory.class.getResourceAsStream("/config-dev.json"),
                                MainConfig.class
                        );
            }
        }
        return mainConfig;
    }
}
