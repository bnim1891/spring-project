package edu.bbte.idde.bnim1891.backend.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Computer extends BaseEntity {
    private String brandName;
    private Double price;
    private Long ssdId;

}
