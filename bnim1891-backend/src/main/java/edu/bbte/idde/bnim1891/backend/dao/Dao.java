package edu.bbte.idde.bnim1891.backend.dao;

import java.util.Collection;

public interface Dao<T> {
    void insert(T entity);

    void delete(long id);

    T read(long id);

    void update(long id, T entity);

    Collection<T> readAll();

}

