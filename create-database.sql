create database hardwareshop;

use hardwareshop;

create table if not exists Ssd(
	ssdId bigint auto_increment,
    brandName varchar(20),
    price double,
    storageCapacity int, 
    readSpeed int, 
    writeSpeed int,
    primary key (ssdId)
);

create table if not exists Computer(
	computerId bigint auto_increment,
	brandName varchar(20),
    price double,
    ssdId bigint,
    primary key (computerId),
    foreign key (ssdId) references Ssd(ssdId)
);

insert into Ssd(brandName, price, storageCapacity, readSpeed, writeSpeed) values ( "OtherBrand", 123.45, 123, 234, 345);
insert into Ssd(brandName, price, storageCapacity, readSpeed, writeSpeed) values ("Adata", 123.45, 123, 234, 345);
insert into Ssd(brandName, price, storageCapacity, readSpeed, writeSpeed) values ("Kingston", 123.45, 123, 234, 345);

insert into Computer( brandName, price, ssdId) values ( "Dell", 1994.5, 1);
insert into Computer( brandName, price, ssdId) values ( "Lenovo", 1834.5, 3);
insert into Computer( brandName, price, ssdId) values ( "Asus", 1234.5, 1);

select * from Ssd

select max(ssdId) from Ssd 

select * from Ssd where ssdId = (select max(ssdId) from Ssd)