package edu.bbte.idde.bnim1891.dao;

import edu.bbte.idde.bnim1891.model.Ssd;

import java.util.Collection;

public interface SsdDao extends Dao<Ssd> {
    Collection<Ssd> findByPrice(Double price);
}
