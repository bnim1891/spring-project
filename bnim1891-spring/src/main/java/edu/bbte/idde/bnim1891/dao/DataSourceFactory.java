package edu.bbte.idde.bnim1891.dao;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;
import java.io.IOException;

@Configuration
@Profile("!mem")
public class DataSourceFactory {
    @Value("${jdbc_url}")
    private String url;
    @Value("${jdbc_driver}")
    private String driver;
    @Value("${jdbc_user:root}")
    private String user;
    @Value("${jdbc_password:root}")
    private String password;
    @Value("${jdbc_poolsize}")
    private Integer poolsize;

    @Bean
    @Primary
    public DataSource getDataSource() throws IOException {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setJdbcUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        dataSource.setMaximumPoolSize(poolsize);
        return dataSource;
    }
}
