package edu.bbte.idde.bnim1891.dao;

import edu.bbte.idde.bnim1891.model.Computer;

public interface ComputerDao extends Dao<Computer> {
    Computer findByPrice(Double price);
}
