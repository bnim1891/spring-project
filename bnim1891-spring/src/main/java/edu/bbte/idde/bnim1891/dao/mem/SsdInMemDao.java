package edu.bbte.idde.bnim1891.dao.mem;

import edu.bbte.idde.bnim1891.dao.SsdDao;
import edu.bbte.idde.bnim1891.model.Ssd;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
@Profile("mem")
public class SsdInMemDao extends AbstractInMemDao<Ssd> implements SsdDao {

    public SsdInMemDao() {
        super();
        log = LoggerFactory.getLogger(SsdInMemDao.class);
        log.info("SsdInMemDao constructor executed");
        database = new ConcurrentHashMap<>();
        primaryKey = new AtomicLong(0L);
        this.saveAndFlush(new Ssd("Kingston",158.85,240,500,350, new ArrayList<>()));
        this.saveAndFlush(new Ssd("ADATA", 102.00, 120, 520, 320, new ArrayList<>()));
        this.saveAndFlush(new Ssd("Kingston", 200.99, 140, 530, 340, new ArrayList<>()));
    }

    @Override
    public Collection<Ssd> findByPrice(Double price) {
        Collection<Ssd> collection = database.values();
        ArrayList<Ssd> arrayList = new ArrayList<>();
        for (Ssd ssd :collection) {
            if (ssd.getPrice().equals(price)) {
                arrayList.add(ssd);
            }
        }
        log.info("findByPrice executed");
        return arrayList;
    }
}
