package edu.bbte.idde.bnim1891.dao.jpa;

import edu.bbte.idde.bnim1891.dao.SsdDao;
import edu.bbte.idde.bnim1891.model.Ssd;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SsdJpaDao extends SsdDao,  JpaRepository<Ssd, Long> {
}
