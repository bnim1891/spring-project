package edu.bbte.idde.bnim1891.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "jpa_ssd")
public class Ssd extends BaseEntity {
    private String brandName;
    private Double price;
    private Integer storageCapacity;
    private Integer readSpeed;
    private Integer writeSpeed;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    @ToString.Exclude
    private Collection<Computer> computerCollection;
}
