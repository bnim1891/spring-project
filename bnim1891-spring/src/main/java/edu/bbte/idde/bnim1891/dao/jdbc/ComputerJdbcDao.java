package edu.bbte.idde.bnim1891.dao.jdbc;

import edu.bbte.idde.bnim1891.dao.ComputerDao;
import edu.bbte.idde.bnim1891.dao.Dao;
import edu.bbte.idde.bnim1891.DaoException;
import edu.bbte.idde.bnim1891.model.Computer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.Collection;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Profile("jdbc")
@Repository
public class ComputerJdbcDao implements ComputerDao, Dao<Computer> {

    public static final Logger LOG = LoggerFactory.getLogger(ComputerJdbcDao.class);
    @Autowired
    private DataSource dataSource;

    @Override
    public Computer saveAndFlush(Computer entity) {
        //     Computer computer = null;
        //     try (Connection connection = dataSource.getConnection()) {
        //         String query =
        //                 "insert into Computer(brandName, price, ssdId) values(?, ?, ?)";
        //         PreparedStatement preparedStatement = connection.prepareStatement(query);
        //         preparedStatement.setString(1, entity.getBrandName());
        //         preparedStatement.setDouble(2, entity.getPrice());
        //         preparedStatement.executeUpdate();
        //
        //         query = "select * from Ssd where ssdId = (select max(ssdId) from Ssd)";
        //         preparedStatement = connection.prepareStatement(query);
        //         ResultSet set = preparedStatement.executeQuery();
        //         if (set.next()) {
        //             computer = new Computer(set.getString(2), set.getDouble(3));
        //             computer.setId(set.getLong(1));
        //         }
        //     } catch (SQLException e) {
        //         LOG.error(String.valueOf(e));
        //     }
        //     return computer;
        return null;
    }

    @Override
    public void deleteById(long id) {

        try (Connection connection = dataSource.getConnection()) {
            String query = "delete from Computer where computerId = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
            throw new DaoException("database error", e);
        }
    }

    @Override
    public Optional<Computer> findById(long id) {

        try (Connection connection = dataSource.getConnection()) {
            String query =
                    "select computerId, brandName, price, ssdId from Computer where computerId = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setLong(1, id);
                ResultSet set = preparedStatement.executeQuery();
                if (set.next()) {
                    Computer computer = new Computer();
                    computer.setBrandName(set.getString(2));
                    computer.setPrice(set.getDouble(3));
                    computer.setId(set.getLong(1));
                    return Optional.ofNullable(computer);
                }
            }

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
            throw new DaoException("database error", e);

        }
        return Optional.empty();
    }

    @Override
    public Computer save(Computer entity) {

        //     try (Connection connection = dataSource.getConnection()) {
        //         String query =
        //                 "update Computer set brandName = ?, price = ?, ssdId = ? where computerId = ?";
        //         try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
        //             preparedStatement.setString(1, entity.getBrandName());
        //             preparedStatement.setDouble(2, entity.getPrice());
        //             preparedStatement.setLong(4, entity.getId());
        //             preparedStatement.executeUpdate();
        //         }
        //        } catch (SQLException e) {
        //            LOG.error(String.valueOf(e));
        //      }
        return null;
    }

    @Override
    public Collection<Computer> findAll() {

        Collection<Computer> computers = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {

            String query = "select * from Computer";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                ResultSet set = preparedStatement.executeQuery();
                while (set.next()) {
                    Computer computer = new Computer();
                    computer.setBrandName(set.getString(2));
                    computer.setPrice(set.getDouble(3));
                    computer.setId(set.getLong(1));
                    computers.add(computer);
                }
            }

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
            throw new DaoException("database error", e);
        }
        return computers;
    }

    @Override
    public Computer findByPrice(Double price) {

        try (Connection connection = dataSource.getConnection()) {
            String query = "select * from Computer where price = ?";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setDouble(1, price);
                ResultSet set = preparedStatement.executeQuery();
                if (set.next()) {
                    Computer computer = new Computer();
                    computer.setBrandName(set.getString(2));
                    computer.setPrice(set.getDouble(3));
                    computer.setId(set.getLong(1));
                    return computer;
                }
            }

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
            throw new DaoException("database error", e);
        }
        return null;
    }
}
