package edu.bbte.idde.bnim1891.dao.mem;

import edu.bbte.idde.bnim1891.dao.ComputerDao;
import edu.bbte.idde.bnim1891.model.Computer;
import edu.bbte.idde.bnim1891.model.Ssd;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
@Profile("mem")
public class ComputerInMemDao extends AbstractInMemDao<Computer> implements ComputerDao {

    public ComputerInMemDao() {
        super();
        database = new ConcurrentHashMap<>();
        primaryKey = new AtomicLong(0L);
        log = LoggerFactory.getLogger(ComputerInMemDao.class);
        log.info("ComputerInMemDao constructor executed");

        this.saveAndFlush(new Computer("Asus", 23488.90,
                new Ssd("ADATA", 102.00, 120, 520, 320, new ArrayList<>())));
        this.saveAndFlush(new Computer("Dell", 2560.90,
                new Ssd("ADATA", 102.00, 120, 520, 320, new ArrayList<>())));
        this.saveAndFlush(new Computer("Lenovo", 4580.90,
                new Ssd("ADATA", 102.00, 120, 520, 320, new ArrayList<>())));
    }

    @Override
    public Computer findByPrice(Double price) {
        Collection<Computer> collection = database.values();
        for (Computer computer :collection) {
            if (computer.getPrice().equals(price)) {
                return computer;
            }
        }
        log.info("findByPrice executed");
        return null;
    }
}
