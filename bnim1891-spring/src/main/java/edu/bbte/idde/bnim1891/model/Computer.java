package edu.bbte.idde.bnim1891.model;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Entity
@Table(name = "jpa_computer")
public class Computer extends BaseEntity {
    private String brandName;
    private Double price;
    @Transient
    private Ssd ssd;
}
