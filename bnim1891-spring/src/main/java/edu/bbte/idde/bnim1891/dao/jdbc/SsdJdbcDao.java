package edu.bbte.idde.bnim1891.dao.jdbc;

import edu.bbte.idde.bnim1891.DaoException;
import edu.bbte.idde.bnim1891.dao.SsdDao;
import edu.bbte.idde.bnim1891.model.Ssd;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import javax.sql.DataSource;

@Profile("jdbc")
@Repository
public class SsdJdbcDao implements SsdDao {

    public static final Logger LOG = LoggerFactory.getLogger(SsdJdbcDao.class);
    @Autowired
    private DataSource dataSource;

    @Override
    public Ssd saveAndFlush(Ssd entity) {

        try (Connection connection = dataSource.getConnection()) {
            String query =
                    "insert into Ssd(brandName, price, storageCapacity,"
                            + " readSpeed, writeSpeed) values(?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement =
                    connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, entity.getBrandName());
            preparedStatement.setDouble(2, entity.getPrice());
            preparedStatement.setInt(3, entity.getStorageCapacity());
            preparedStatement.setInt(4, entity.getReadSpeed());
            preparedStatement.setInt(5, entity.getWriteSpeed());
            preparedStatement.executeUpdate();
            LOG.info("insert");
            ResultSet newIdSet = preparedStatement.getGeneratedKeys();
            if (newIdSet.next()) {
                Long id = newIdSet.getLong(1);
                entity.setId(id);
            }

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
            throw new DaoException("database error", e);
        }
        return entity;
    }

    @Override
    public void deleteById(long id) {

        try (Connection connection = dataSource.getConnection()) {
            String query = "delete from Ssd where ssdId = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            LOG.info("delete");
        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
            throw new DaoException("database error", e);
        }
    }

    @Override
    public Optional<Ssd> findById(long id) {

        try (Connection connection = dataSource.getConnection()) {
            String query = "select ssdId, brandName, price, storageCapacity, readSpeed, writeSpeed"
                    + " from Ssd where ssdId = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, id);
            ResultSet set = preparedStatement.executeQuery();
            if (set.next()) {
                Ssd ssd = new Ssd();
                ssd.setBrandName(set.getString(2));
                ssd.setPrice(set.getDouble(3));
                ssd.setStorageCapacity(set.getInt(4));
                ssd.setReadSpeed(set.getInt(5));
                ssd.setWriteSpeed(set.getInt(6));
                ssd.setId(set.getLong(1));
                return Optional.of(ssd);
            }
            LOG.info("read");

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
            throw new DaoException("database error", e);
        }
        return Optional.empty();
    }

    @Override
    public Ssd save(Ssd entity) {

        try (Connection connection = dataSource.getConnection()) {
            String query =
                    "update Ssd set brandName = ?, price = ?, storageCapacity = ?,"
                            + " readSpeed = ?, writeSpeed = ? where ssdId = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, entity.getBrandName());
            preparedStatement.setDouble(2, entity.getPrice());
            preparedStatement.setInt(3, entity.getStorageCapacity());
            preparedStatement.setInt(4, entity.getReadSpeed());
            preparedStatement.setInt(5, entity.getWriteSpeed());
            preparedStatement.setLong(6, entity.getId());
            preparedStatement.executeUpdate();
            LOG.info("update");
        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
            throw new DaoException("database error", e);
        }
        return entity;
    }

    @Override
    public Collection<Ssd> findAll() {

        Collection<Ssd> ssds = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {

            String query = "select * from Ssd";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet set = preparedStatement.executeQuery();
            while (set.next()) {
                Ssd ssd = new Ssd();
                ssd.setBrandName(set.getString(2));
                ssd.setPrice(set.getDouble(3));
                ssd.setStorageCapacity(set.getInt(4));
                ssd.setReadSpeed(set.getInt(5));
                ssd.setWriteSpeed(set.getInt(6));
                ssd.setId(set.getLong(1));
                ssds.add(ssd);
            }
            LOG.info("readAll");

        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
            throw new DaoException("database error", e);
        }
        return ssds;
    }

    @Override
    public Collection<Ssd> findByPrice(Double price) {

        ArrayList<Ssd> arrayList = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            String query = "select * from Ssd where price = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setDouble(1, price);
            ResultSet set = preparedStatement.executeQuery();
            if (set.next()) {
                Ssd ssd = new Ssd();
                ssd.setBrandName(set.getString(2));
                ssd.setPrice(set.getDouble(3));
                ssd.setStorageCapacity(set.getInt(4));
                ssd.setReadSpeed(set.getInt(5));
                ssd.setWriteSpeed(set.getInt(6));
                ssd.setId(set.getLong(1));
                arrayList.add(ssd);
            }
            LOG.info("findByPrice");
        } catch (SQLException e) {
            LOG.error(String.valueOf(e));
            throw new DaoException("database error", e);
        }
        return arrayList;
    }
}
