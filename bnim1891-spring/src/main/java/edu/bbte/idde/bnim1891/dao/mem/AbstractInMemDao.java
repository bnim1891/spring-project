package edu.bbte.idde.bnim1891.dao.mem;

import edu.bbte.idde.bnim1891.dao.Dao;
import edu.bbte.idde.bnim1891.model.BaseEntity;
import org.slf4j.Logger;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public abstract class AbstractInMemDao<T extends BaseEntity> implements Dao<T> {
    protected ConcurrentHashMap<Long, T> database;
    protected AtomicLong primaryKey;
    protected Logger log;

    @Override
    public T saveAndFlush(T entity) {
        log.info("insert executed");
        entity.setId(primaryKey.getAndIncrement());
        database.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public void deleteById(long id) {
        log.info("delete executed");
        database.remove(id);
    }

    @Override
    public Optional<T> findById(long id) {
        log.info("read executed");
        return Optional.ofNullable(database.get(id));
    }

    @Override
    public T save(T entity) {
        log.info("update executed");
        database.computeIfPresent(entity.getId(), (key, val) -> entity);
        return entity;
    }

    @Override
    public Collection<T> findAll() {
        log.info("readAll executed");
        return database.values();
    }

}
