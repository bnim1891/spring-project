package edu.bbte.idde.bnim1891.mapper;

import edu.bbte.idde.bnim1891.dto.incoming.SsdCreationDto;
import edu.bbte.idde.bnim1891.dto.outgoing.SsdDetailedDto;
import edu.bbte.idde.bnim1891.model.Ssd;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class SsdMapper {
    public abstract Ssd dtoToModel(SsdCreationDto dto);

    public abstract SsdDetailedDto modelToDto(Ssd ssd);

    @IterableMapping(elementTargetType = SsdDetailedDto.class)
    public abstract Collection<SsdDetailedDto> modelsToDtos(Collection<Ssd> ssds);

}
