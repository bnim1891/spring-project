package edu.bbte.idde.bnim1891.controller;

import edu.bbte.idde.bnim1891.controller.exception.NotFoundException;
import edu.bbte.idde.bnim1891.dao.SsdDao;
import edu.bbte.idde.bnim1891.dto.incoming.SsdCreationDto;
import edu.bbte.idde.bnim1891.dto.outgoing.SsdDetailedDto;
import edu.bbte.idde.bnim1891.mapper.SsdMapper;
import edu.bbte.idde.bnim1891.model.Ssd;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.Collection;

@Slf4j
@Controller
@RequestMapping("/ssds")
public class SsdController {
    @Autowired
    SsdDao ssdDao;

    @Autowired
    SsdMapper ssdMapper;

    @GetMapping
    @ResponseBody
    public Collection<SsdDetailedDto> findAll(@RequestParam(value = "price", required = false) Double price)
            throws SQLException {
        if (price == null) {
            return ssdMapper.modelsToDtos(ssdDao.findAll());
        }
        log.info("findAll executed");

        return ssdMapper.modelsToDtos(ssdDao.findByPrice(price));
    }

    @GetMapping("/{id}")
    @ResponseBody
    public SsdDetailedDto getById(@PathVariable("id") Long id) {
        Ssd ssd = ssdDao.findById(id).orElseThrow(NotFoundException::new);
        if (ssd == null) {
            throw new NotFoundException();
        }
        log.info("getById executed");
        return ssdMapper.modelToDto(ssd);
    }

    @PostMapping
    @ResponseBody
    public SsdDetailedDto create(@RequestBody @Valid SsdCreationDto ssdCreationDto) {
        Ssd ssd = ssdMapper.dtoToModel(ssdCreationDto);
        Ssd newSsd = ssdDao.saveAndFlush(ssd);
        log.info("create executed");
        return ssdMapper.modelToDto(newSsd);
    }

    @PutMapping("/{id}")
    @ResponseBody
    public void update(@RequestBody @Valid SsdCreationDto ssdCreationDto,
                       @PathVariable("id") Long id) {
        Ssd ssd = ssdDao.findById(id).orElseThrow(NotFoundException::new);
        if (ssd == null) {
            throw new NotFoundException();
        }
        Ssd newSsd = ssdMapper.dtoToModel(ssdCreationDto);
        newSsd.setId(id);
        ssdDao.save(newSsd);
        log.info("update executed");
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public void delete(@PathVariable("id") Long id) {
        Ssd ssd;
        ssd = ssdDao.findById(id).orElseThrow(NotFoundException::new);
        if (ssd == null) {
            throw new NotFoundException();
        }
        ssdDao.deleteById(id);
        log.info("delete executed");
    }
}
