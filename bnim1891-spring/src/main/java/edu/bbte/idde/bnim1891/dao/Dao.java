package edu.bbte.idde.bnim1891.dao;

import java.util.Collection;
import java.util.Optional;

public interface Dao<T> {
    T saveAndFlush(T entity);

    void deleteById(long id);

    Optional<T> findById(long id);

    T save(T entity);

    Collection<T> findAll();

}

