package edu.bbte.idde.bnim1891.dto.incoming;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class SsdCreationDto {
    @NotNull
    @Size(max = 50)
    private String brandName;
    @NotNull
    private Double price;
    @NotNull
    @Positive
    private Integer storageCapacity;
    @NotNull
    @Positive
    private Integer readSpeed;
    @NotNull
    @Positive
    private Integer writeSpeed;
}
