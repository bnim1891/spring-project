package edu.bbte.idde.bnim1891.mapper;

import edu.bbte.idde.bnim1891.dto.incoming.ComputerCreationDto;
import edu.bbte.idde.bnim1891.dto.outgoing.ComputerDto;
import edu.bbte.idde.bnim1891.dto.outgoing.SsdDetailedDto;
import edu.bbte.idde.bnim1891.model.Computer;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class ComputerMapper {
    public abstract Computer dtoToModel(ComputerCreationDto dto);

    public abstract ComputerDto modelToDto(Computer computer);

    @IterableMapping(elementTargetType = SsdDetailedDto.class)
    public abstract Collection<ComputerDto> modelsToDtos(Collection<Computer> computers);
}
