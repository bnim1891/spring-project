package edu.bbte.idde.bnim1891.dto.outgoing;

import lombok.Data;

@Data
public class ComputerDto {
    private Long id;
    private String brandName;
    private Double price;
}
