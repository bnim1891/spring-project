package edu.bbte.idde.bnim1891.dto.incoming;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class ComputerCreationDto {
    @NotNull
    @Size(max = 50)
    private String brandName;
    @NotNull
    private Double price;
}
