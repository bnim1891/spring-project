package edu.bbte.idde.bnim1891.dto.outgoing;

import lombok.Data;

@Data
public class SsdDetailedDto {
    private Long id;
    private String brandName;
    private Double price;
    private Integer storageCapacity;
    private Integer readSpeed;
    private Integer writeSpeed;
}
