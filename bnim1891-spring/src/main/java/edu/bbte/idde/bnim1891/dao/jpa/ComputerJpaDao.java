package edu.bbte.idde.bnim1891.dao.jpa;

import edu.bbte.idde.bnim1891.dao.ComputerDao;
import edu.bbte.idde.bnim1891.model.Computer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComputerJpaDao extends ComputerDao, JpaRepository<Computer, Long> {
}
