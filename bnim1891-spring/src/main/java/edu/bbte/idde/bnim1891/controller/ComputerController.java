package edu.bbte.idde.bnim1891.controller;

import edu.bbte.idde.bnim1891.controller.exception.NotFoundException;
import edu.bbte.idde.bnim1891.dao.SsdDao;
import edu.bbte.idde.bnim1891.dto.incoming.ComputerCreationDto;
import edu.bbte.idde.bnim1891.dto.outgoing.ComputerDto;
import edu.bbte.idde.bnim1891.mapper.ComputerMapper;
import edu.bbte.idde.bnim1891.model.Computer;
import edu.bbte.idde.bnim1891.model.Ssd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.Collection;

@RestController
@RequestMapping("/ssds/{id}/computers")
public class ComputerController {
    @Autowired
    private SsdDao ssdDao;
    @Autowired
    private ComputerMapper computerMapper;

    @GetMapping
    public Collection<Computer> findComputersForSsd(@PathVariable("id") Long ssdId) throws SQLException {
        Ssd ssd = ssdDao.findById(ssdId).orElseThrow(NotFoundException::new);
        return ssd.getComputerCollection();
    }

    @PostMapping
    public ComputerDto addComputerToSsd(@PathVariable("id") Long ssdId,
                                        @RequestBody @Valid ComputerCreationDto computerCreationDto)
            throws SQLException {
        Ssd ssd = ssdDao.findById(ssdId).orElseThrow(NotFoundException::new);
        Collection<Computer> computerCollection = ssd.getComputerCollection();
        Computer computer = computerMapper.dtoToModel(computerCreationDto);
        computer.setSsd(ssd);
        computerCollection.add(computer);
        ssd.setComputerCollection(computerCollection);
        ssdDao.save(ssd);
        Object[] array = computerCollection.toArray();
        return computerMapper.modelToDto((Computer) array[computerCollection.size() - 1]);
    }

    @DeleteMapping("/{computerId}")
    public void delete(@PathVariable("id") Long ssdId, @PathVariable("computerId") Long computerId)
            throws SQLException {
        Ssd ssd = ssdDao.findById(ssdId).orElseThrow(NotFoundException::new);
        Collection<Computer> computerCollection = ssd.getComputerCollection();
        computerCollection.removeIf(computer -> computer.getId().equals(computerId));
        ssd.setComputerCollection(computerCollection);
        ssdDao.save(ssd);
    }
}
