<html>
<head>
    <link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div>
    <form method="POST" action="./login" class="form">
        <label for="username" class="form_text">Username:</label><br>
        <input type="text" id="username" name="username" required><br>
        <label for="password" class="form_text">Password:</label><br>
        <input type="password" id="password" name="password" required><br>
        <button type="submit">Log in</button>
    </form>
</div>
</body>
</html>