<html>
<head>
    <link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <#list ssds as ssd>
        <div class='form-text'>
            ${ssd.getId()}: ${ssd.getBrandName()}, ${ssd.getPrice()}, ${ssd.getReadSpeed()}, ${ssd.getWriteSpeed()} <br />
        </div>
    </#list>
    <form method="POST" action="./logout">
        <button type="submit">Log out</button>
    </form>
</body>
</html>