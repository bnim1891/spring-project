package edu.bbte.idde.bnim1891.web;

import edu.bbte.idde.bnim1891.backend.dao.AbstractDaoFactory;
import edu.bbte.idde.bnim1891.backend.dao.SsdDao;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet("/hardwaretemplate")
public class HardwareTemplateServlet extends HttpServlet {
    private final SsdDao ssdDao = AbstractDaoFactory.getDaoFactory().getSsdDao();
    private Template template;

    public HardwareTemplateServlet() throws IOException {
        super();
    }

    @Override
    public void init() throws ServletException {
        Configuration cfg = new Configuration(new Version("2.3.31"));
        cfg.setClassForTemplateLoading(HardwareTemplateServlet.class, "/");
        cfg.setDefaultEncoding("UTF-8");
        try {
            template = cfg.getTemplate("template.ftl");
        } catch (IOException e) {
            throw new ServletException();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Map<String, Object> templateData = new ConcurrentHashMap<>();
        templateData.put("ssds", ssdDao.readAll());
        try {
            template.process(templateData, resp.getWriter());
        } catch (TemplateException e) {
            throw new ServletException();
        }
    }
}
