package edu.bbte.idde.bnim1891.web;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import edu.bbte.idde.bnim1891.backend.dao.AbstractDaoFactory;
import edu.bbte.idde.bnim1891.backend.dao.SsdDao;
import edu.bbte.idde.bnim1891.backend.model.Ssd;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;

@WebServlet("/hardware")
public class HardwareServlet extends HttpServlet {

    private final SsdDao ssdDao = AbstractDaoFactory.getDaoFactory().getSsdDao();
    private final ObjectMapper objectMapper = new ObjectMapper();
    public static final Logger LOG = LoggerFactory.getLogger(HardwareServlet.class);

    public HardwareServlet() throws IOException {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setHeader("Content-Type", "application/json");
        String idStr = req.getParameter("id");
        if (idStr != null) {
            try {
                Integer id = Integer.parseInt(idStr);
                Ssd ssd = (Ssd) ssdDao.read(id);

                if (ssd == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    objectMapper.writeValue(resp.getWriter(), ssd);
                }
                return;
            } catch (NumberFormatException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
        }
        objectMapper.writeValue(resp.getWriter(), ssdDao.readAll());
        LOG.info("doGet method executed");
    }

    protected Boolean checkParts(String brandName, Double price, Integer readSpeed, Integer writeSpeed) {
        return brandName == null || price == null || readSpeed == null || writeSpeed == null;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setHeader("Content-Type", "application/json");
        BufferedReader bufferedReader =  req.getReader();
        try {
            Ssd ssd = objectMapper.readValue(bufferedReader, Ssd.class);
            if (checkParts(ssd.getBrandName(), ssd.getPrice(), ssd.getReadSpeed(), ssd.getWriteSpeed())) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
            ssdDao.insert(ssd);
        } catch (JsonParseException | InvalidFormatException | UnrecognizedPropertyException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        LOG.info("doPost method executed");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setHeader("Content-Type", "application/json");
        String idStr = req.getParameter("id");
        if (idStr != null) {
            try {
                Integer id = Integer.parseInt(idStr);
                Ssd ssd = (Ssd) ssdDao.read(id);

                if (ssd == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    ssdDao.delete(id);
                }
            } catch (NumberFormatException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        LOG.info("doDelete method executed");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setHeader("Content-Type", "application/json");
        String idStr = req.getParameter("id");
        if (idStr != null) {
            try {
                Long id = Long.parseLong(idStr);
                Ssd ssd = (Ssd) ssdDao.read(id);

                if (ssd == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    BufferedReader bufferedReader =  req.getReader();
                    try {
                        Ssd newSsd = objectMapper.readValue(bufferedReader, Ssd.class);
                        if (checkParts(ssd.getBrandName(), ssd.getPrice(), ssd.getReadSpeed(), ssd.getWriteSpeed())) {
                            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        }
                        newSsd.setId(id);
                        ssdDao.update(id, newSsd);
                    } catch (JsonParseException | InvalidFormatException | UnrecognizedPropertyException e) {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }
                }
            } catch (NumberFormatException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        LOG.info("doPut method executed");
    }
}
