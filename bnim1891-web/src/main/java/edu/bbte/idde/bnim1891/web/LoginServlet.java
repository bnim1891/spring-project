package edu.bbte.idde.bnim1891.web;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private Template template;

    @Override
    public void init() throws ServletException {
        Configuration cfg = new Configuration(new Version("2.3.31"));
        cfg.setClassForTemplateLoading(HardwareTemplateServlet.class, "/");
        cfg.setDefaultEncoding("UTF-8");
        try {
            template = cfg.getTemplate("login.ftl");
        } catch (IOException e) {
            throw new ServletException();
        }
    }

    protected void showLoginPage(HttpServletResponse res) throws IOException, ServletException {

        Map<String, Object> templateData = new ConcurrentHashMap<>();
        try {
            template.process(templateData, res.getWriter());
        } catch (TemplateException e) {
            throw new ServletException();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        showLoginPage(resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String enteredUserName = req.getParameter("username");
        String enteredPassword = req.getParameter("password");
        Cookie cookie;
        HttpSession session;

        final String username = "norab";
        final String password = "hello";

        if (username.equals(enteredUserName) && password.equals(enteredPassword)) {
            session = req.getSession();
            session.setAttribute("username", "value");
            session.setMaxInactiveInterval(10 * 60);
            cookie = new Cookie("username", enteredUserName);
            cookie.setMaxAge(10 * 60);
            res.addCookie(cookie);
            res.sendRedirect("./hardwaretemplate");
        } else {
            showLoginPage(res);
        }
    }
}
