package edu.bbte.idde.bnim1891.web;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import edu.bbte.idde.bnim1891.backend.dao.AbstractDaoFactory;
import edu.bbte.idde.bnim1891.backend.dao.ComputerDao;
import edu.bbte.idde.bnim1891.backend.model.Computer;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;

@WebServlet("/computer")
public class ComputerServlet extends HttpServlet {
    private final ComputerDao computerDao = AbstractDaoFactory.getDaoFactory().getComputerDao();
    private final ObjectMapper objectMapper = new ObjectMapper();
    public static final Logger LOG = LoggerFactory.getLogger(HardwareServlet.class);

    public ComputerServlet() throws IOException {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setHeader("Content-Type", "application/json");
        String idStr = req.getParameter("id");
        if (idStr != null) {
            try {
                Integer id = Integer.parseInt(idStr);
                Computer computer = (Computer) computerDao.read(id);

                if (computer == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    objectMapper.writeValue(resp.getWriter(), computer);
                }
                return;
            } catch (NumberFormatException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
        }
        objectMapper.writeValue(resp.getWriter(), computerDao.readAll());
        LOG.info("doGet method executed");
    }

    protected Boolean checkParts(String brandName, Double price, Long ssdId) {
        return brandName == null || price == null || ssdId == null;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setHeader("Content-Type", "application/json");
        BufferedReader bufferedReader =  req.getReader();
        try {
            Computer computer = objectMapper.readValue(bufferedReader, Computer.class);
            if (checkParts(computer.getBrandName(), computer.getPrice(), computer.getSsdId())) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
            computerDao.insert(computer);
        } catch (JsonParseException | InvalidFormatException | UnrecognizedPropertyException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
        LOG.info("doPost method executed");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setHeader("Content-Type", "application/json");
        String idStr = req.getParameter("id");
        if (idStr != null) {
            try {
                Integer id = Integer.parseInt(idStr);
                Computer computer = (Computer) computerDao.read(id);

                if (computer == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    computerDao.delete(id);
                }
            } catch (NumberFormatException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        LOG.info("doDelete method executed");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setHeader("Content-Type", "application/json");
        String idStr = req.getParameter("id");
        if (idStr != null) {
            try {
                Long id = Long.parseLong(idStr);
                Computer computer = (Computer) computerDao.read(id);

                if (computer == null) {
                    resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                } else {
                    BufferedReader bufferedReader =  req.getReader();
                    try {
                        Computer newComputer = objectMapper.readValue(bufferedReader, Computer.class);
                        if (checkParts(computer.getBrandName(), computer.getPrice(), computer.getSsdId())) {
                            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        }
                        newComputer.setId(id);
                        computerDao.update(id, newComputer);
                    } catch (JsonParseException | InvalidFormatException | UnrecognizedPropertyException e) {
                        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    }
                }
            } catch (NumberFormatException e) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        LOG.info("doPut method executed");
    }

}
