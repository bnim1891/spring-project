package edu.bbte.idde.bnim1891.web;

import jakarta.servlet.FilterChain;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebFilter("/hardwaretemplate/*")
public class LogFilter extends HttpFilter {
    private static final Logger LOG = LoggerFactory.getLogger(LogFilter.class);

    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res,
                            FilterChain chain) throws  ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session == null || session.getAttribute("username") == null) {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("./login");
            requestDispatcher.forward(req, res);
        } else {
            chain.doFilter(req, res);
        }
        LOG.info("doFilter executed");
    }
}