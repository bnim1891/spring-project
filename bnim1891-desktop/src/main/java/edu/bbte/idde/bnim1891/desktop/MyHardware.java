package edu.bbte.idde.bnim1891.desktop;

import edu.bbte.idde.bnim1891.backend.dao.AbstractDaoFactory;
import edu.bbte.idde.bnim1891.backend.dao.ComputerDao;
import edu.bbte.idde.bnim1891.backend.dao.SsdDao;
import edu.bbte.idde.bnim1891.backend.model.Computer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import edu.bbte.idde.bnim1891.backend.model.BaseEntity;
import edu.bbte.idde.bnim1891.backend.model.Ssd;

import java.io.IOException;

public class MyHardware {

    public static final Logger LOG = LoggerFactory.getLogger(MyHardware.class);

    public static void main(String[] args) throws IOException {
        SsdDao ssdDao = AbstractDaoFactory.getDaoFactory().getSsdDao();
        ssdDao.insert(new Ssd("zoltan",158.85,240,500,350));
        for (BaseEntity ssd : ssdDao.readAll()) {
            LOG.debug(ssd.toString());
        }
        ssdDao.update(1, new Ssd("dora",178.95,240,500,350));
        LOG.debug(ssdDao.read(1).toString());
        ssdDao.delete(2);
        for (BaseEntity ssd : ssdDao.readAll()) {
            LOG.debug(ssd.toString());
        }

        ComputerDao computerDao = AbstractDaoFactory.getDaoFactory().getComputerDao();
        computerDao.insert(new Computer("nagyondraga", 10000.0, 1L));
        for (BaseEntity computer : computerDao.readAll()) {
            LOG.debug(computer.toString());
        }
        computerDao.update(1, new Computer("nagyonolcso", 10.0, 1L));
        LOG.debug(computerDao.read(1).toString());
        computerDao.delete(2);
        for (BaseEntity computer : computerDao.readAll()) {
            LOG.debug(computer.toString());
        }
    }

}
